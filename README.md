    

Esta opción nos permite iniciar un grupo de 5 contenedores de una aplicación, utilizando la herramienta de Docker Compose. Esto es útil cuando queremos probar cómo nuestra aplicación escala horizontalmente, es decir, cómo se comporta al aumentar el número de instancias de la aplicación. El comando utilizado es el siguiente:

docker-compose up -d --scale web=5

    

Una vez que hemos iniciado nuestros contenedores, podemos acceder a nuestra aplicación a través del navegador web. El comando anterior nos indica que debemos ingresar en la dirección http://localhost:4000 para ver la aplicación en acción. Es importante destacar que este puerto debe estar disponible en nuestro host local, es decir, que no esté siendo utilizado por otra aplicación.

    
Cuando terminamos de trabajar con nuestros contenedores, podemos apagarlos utilizando el comando docker-compose down. Este comando detiene y elimina todos los contenedores, redes y volúmenes creados por docker-compose. Es importante destacar que si utilizamos la opción -v, también eliminaremos los volúmenes asociados a nuestros contenedores. El comando utilizado es el siguiente:

docker-compose down


Para construir una imagen de nuestra aplicación, debemos tener un archivo Dockerfile que defina cómo se construirá dicha imagen. Luego, utilizaremos el comando docker build para crear la imagen, y la opción -t para darle un nombre y etiqueta a la imagen. Es importante destacar que el punto al final del comando indica que el contexto de construcción es el directorio actual. El comando utilizado es el siguiente:

docker build -t friendlyhello .


Por último, si queremos ejecutar un contenedor de nuestra aplicación sin el uso de redis, podemos utilizar el siguiente comando. La opción --rm indica que el contenedor se eliminará automáticamente al detenerse, mientras que la opción -p indica que se debe mapear el puerto 4000 del contenedor al puerto 80 del host local. El nombre friendlyhello hace referencia a la imagen que queremos utilizar para crear el contenedor.

docker run --rm -p 4000:80 friendlyhello
